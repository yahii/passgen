<?php
include '../class/Passgenerator.php';
$passgenerator = new Passgenerator();
$list = $passgenerator->generate();
$head = $passgenerator->getHead();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Test</title>
    <link rel="stylesheet" href="./css/css.css">
</head>
<body>
<table class="content">
    <tbody>
    <tr><?php
        foreach ($head as $headName)
        {
            echo '<td>';
            echo $headName;
            echo '</td>';
        }
        ?>
    </tr>
    <?php
    foreach ($list as $k => $row)
    {
        echo '<tr>';
        echo '<td>';
        echo($k + 1);
        echo '</td>';
        foreach ($row as $char)
        {
            echo '<td>';
            echo $char;
            echo '</td>';
        }
        echo '</tr>';
    }
    ?>
    </tbody>
</table>
</body>
</html>