<?php
/**
 * Created by PhpStorm.
 * User: centos
 * Date: 2/19/19
 * Time: 11:53 PM
 */

class Passgenerator
{
    private const ROW_LENGTH = 50;

    private $lowerCase = 'abcdefghijklmnopqrstuvwxyz';
    private $upperCase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    private $numbers = '1234567890';
    private $symbols = '!?~@#-+<>[]{}$%^&*()';

    public function generate(): array
    {
        $list = [];

        $y = 50;
        for ($i = 0; $i < 1500; $i++)
        {
            $t = floor($i / $y);
            $list[$t][] = $this->getRandomSymbol();
        }

        return $list;
    }

    private function getRandomSymbol(): string
    {
        $action = rand(0, 3);

        switch ($action)
        {
            case 0:
                $char = $this->getRandomCharFromArr($this->lowerCase);
                break;
            case 1:
                $char = $this->getRandomCharFromArr($this->numbers);
                break;
            case 2:
                $char = $this->getRandomCharFromArr($this->symbols);
                break;
            case 3:
                $char = $this->getRandomCharFromArr($this->upperCase);
                break;
        }

        return $char;
    }

    /**
     * @return array
     */
    public function getHead(): array
    {
        $header = [];

        for ($i = 0; $i <= self::ROW_LENGTH; $i++)
        {
            $header[] = $i;
        }

        return $header;
    }

    /**
     * @return mixed
     */
    private function getRandomCharFromArr(string $string): string
    {
        $lcaseLen = strlen($string);
        $coord = rand(0, $lcaseLen);
        $char = $string{$coord};
        return $char;
    }
}